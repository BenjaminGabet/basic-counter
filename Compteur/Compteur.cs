﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basicCounteur
{
    public class Compteur
    {
        private static int Total = 0 ;

        public static int incrementation()
        {
            return ++Total;
        }

        public static int decrementation()
        {
            return --Total;
        }

        public static int remiseAZero()
        {
            Total = 0;
            return Total;
        }

        public static int getTotal()
        {
            return Total;
        }
    }
}
