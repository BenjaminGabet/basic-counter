﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using basicCounteur;

namespace basicCounterTest
{
    [TestClass]
    public class CompteurTest
    {
        [TestMethod]
        public void TestIncrement()
        {
           
            Assert.AreEqual(1, Compteur.incrementation());
            Compteur.incrementation();
            Assert.AreEqual(3, Compteur.incrementation());
        }

        [TestMethod]
        public void TestDecrement()
        {
            Compteur.remiseAZero();
            Assert.AreEqual(-1, Compteur.decrementation());
            Compteur.decrementation();
            Assert.AreEqual(-3, Compteur.decrementation());
        }

        [TestMethod]
        public void TestRemiseAZero()
        {
            Compteur.incrementation();
            Assert.AreEqual(0, Compteur.remiseAZero());
        }

        [TestMethod]
        public void TestGetTotal()
        {
            Assert.AreEqual(0, Compteur.getTotal());
            Compteur.incrementation();
            Assert.AreEqual(1, Compteur.getTotal());
        }

    }
}
