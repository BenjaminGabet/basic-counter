﻿Imports basicCounteur


Public Class Form1
    Private Sub btn_plus_Click(sender As Object, e As EventArgs) Handles btn_plus.Click
        lbl_compteur.Text = basicCounteur.Compteur.incrementation()
    End Sub

    Private Sub btn_moins_Click(sender As Object, e As EventArgs) Handles btn_moins.Click
        lbl_compteur.Text = basicCounteur.Compteur.decrementation()
    End Sub

    Private Sub btn_raz_Click(sender As Object, e As EventArgs) Handles btn_raz.Click
        lbl_compteur.Text = basicCounteur.Compteur.remiseAZero()
    End Sub
End Class
