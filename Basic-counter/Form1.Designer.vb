﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btn_moins = New System.Windows.Forms.Button()
        Me.btn_plus = New System.Windows.Forms.Button()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.llb_total = New System.Windows.Forms.Label()
        Me.lbl_compteur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_moins
        '
        Me.btn_moins.Location = New System.Drawing.Point(70, 175)
        Me.btn_moins.Name = "btn_moins"
        Me.btn_moins.Size = New System.Drawing.Size(111, 72)
        Me.btn_moins.TabIndex = 0
        Me.btn_moins.Text = "-"
        Me.btn_moins.UseVisualStyleBackColor = True
        '
        'btn_plus
        '
        Me.btn_plus.Location = New System.Drawing.Point(600, 175)
        Me.btn_plus.Name = "btn_plus"
        Me.btn_plus.Size = New System.Drawing.Size(111, 72)
        Me.btn_plus.TabIndex = 1
        Me.btn_plus.Text = "+"
        Me.btn_plus.UseVisualStyleBackColor = True
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(335, 300)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(111, 72)
        Me.btn_raz.TabIndex = 2
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'llb_total
        '
        Me.llb_total.AutoSize = True
        Me.llb_total.Location = New System.Drawing.Point(362, 134)
        Me.llb_total.Name = "llb_total"
        Me.llb_total.Size = New System.Drawing.Size(54, 17)
        Me.llb_total.TabIndex = 3
        Me.llb_total.Text = "TOTAL"
        '
        'lbl_compteur
        '
        Me.lbl_compteur.AutoSize = True
        Me.lbl_compteur.Location = New System.Drawing.Point(365, 203)
        Me.lbl_compteur.Name = "lbl_compteur"
        Me.lbl_compteur.Size = New System.Drawing.Size(16, 17)
        Me.lbl_compteur.TabIndex = 4
        Me.lbl_compteur.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lbl_compteur)
        Me.Controls.Add(Me.llb_total)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.btn_plus)
        Me.Controls.Add(Me.btn_moins)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_moins As Button
    Friend WithEvents btn_plus As Button
    Friend WithEvents btn_raz As Button
    Friend WithEvents llb_total As Label
    Friend WithEvents lbl_compteur As Label

End Class
